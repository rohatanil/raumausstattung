package Raumausstattung;

import java.util.ArrayList;

public abstract class Moebel {

	private Material material;
	private Design design;
	private static int counter = 1000;
	private final String moebelID;
	private String vorzeichen ="";
	
	public Moebel(Material material, Design design) {
		this.material = material;
		this.design = design;
		vorzeichen = vorzeichen();
		this.moebelID = vorzeichen + counter;
		counter++;
	}
	
	public abstract String vorzeichen();
	public abstract void printDaten();
	
	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public Design getDesign() {
		return design;
	}

	public void setDesign(Design design) {
		this.design = design;
	}

	public String getVorzeichen() {
		return vorzeichen;
	}

	public void setVorzeichen(String vorzeichen) {
		this.vorzeichen = vorzeichen;
	}

	public String getMoebelID() {
		return moebelID;
	}
	
	
}
