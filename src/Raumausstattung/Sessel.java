package Raumausstattung;

import java.util.ArrayList;
import java.util.Iterator;

public class Sessel extends Moebel {

	private final boolean gepolstert; //obs gepolstert ist oder nicht kann nur beim initialen anlegen entschieden werden.
	private boolean armlehnen; //zu armlehnen gibt es setter und getter, weil diese im nachhinein immer wieder entfernt/hinzugefügt werden können.
	private static ArrayList<Sessel> sessellager = new ArrayList<>();//Alle Sessel

	
	public Sessel(Material material, Design design, boolean gepolstert, boolean armlehnen) {
		super(material, design);
		this.gepolstert = gepolstert;
		this.armlehnen = armlehnen;
		sessellager.add(this);
	}
	
	public Sessel(Material material, Design design) {
		super(material, design);
		this.armlehnen = false;
		this.gepolstert = true;
		sessellager.add(this);
	}

	public static void inventarSessel() {
		int holz = 0;
		int metall = 0;
		int plastik = 0;
		
		for(Sessel s : sessellager) {
			if(s.getMaterial().equals(Material.Holz)){
				holz++;
			}
			if(s.getMaterial().equals(Material.Metall)){
				metall++;
			}
			if(s.getMaterial().equals(Material.Plastik)){
				plastik++;
			}
		}
		System.out.println(holz + " Holz Sessel. \n" + metall + " Metall Sessel. \n" + plastik + " Plastik Sessel.");
	}

	
	public static ArrayList<Sessel> getLager() {
		return sessellager;
	}

	public static void setLager(ArrayList<Sessel> lager) {
		Sessel.sessellager = lager;
	}
	
	@Override
	public String vorzeichen() {
		return "S";
	}
	
	@Override
	public void printDaten() {
		System.out.println("ID: " + getMoebelID());
	}
	
	public boolean isArmlehnen() {
		return armlehnen;
	}

	public static ArrayList<Sessel> getAlleSesselImLager() {
		return sessellager;
	}
	
	public void setArmlehnen(boolean armlehnen) {
		this.armlehnen = armlehnen;
	}

	public boolean isGepolstert() {
		return gepolstert;
	}
	
	@Override
	public String toString() {
		return "Sessel [gepolstert=" + gepolstert + ", armlehnen=" + armlehnen + ", "
				+ (getMaterial() != null ? "getMaterial()=" + getMaterial() + ", " : "")
				+ (getDesign() != null ? "getDesign()=" + getDesign() : "") + "]";
	}
	
	

}
