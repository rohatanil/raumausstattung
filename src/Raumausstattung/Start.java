package Raumausstattung;

public class Start {

	public static void main(String[] args) {
		
		Sessel s = new Sessel(Material.Holz, Design.antick, true, true);
		Sessel s2 = new Sessel(Material.Metall, Design.postmodern, true, true);
		Sessel s3 = new Sessel(Material.Plastik, Design.modern, true, true);
		Sessel s4 = new Sessel(Material.Plastik, Design.modern, true, true);
		Tisch t = new Tisch(Material.Holz, Design.klassisch, 50, 3,0.5);
		Tisch t2 = new Tisch(Material.Metall, Design.klassisch, 50, 3,0.5);
		Raum r1 = new Raum(7, "Rohats Raum");
		
		//Exception tests:
		Tisch t5 = new Tisch (Material.Holz, Design.modern, 3,0.8,0.3);
		Raum r2 = new Raum(4, "Rohats Raum"); //hier kommt es zu einer exception wenn ich diese nauch "Rohats Raum" nennen w�rde!
		Raum r3 = new Raum(5, "    "); //Exception test 
		Raum r4 = new Raum(0, "neuer Raum"); //ebenfalls Exception, wiel nutzfl�che nicht 0 sein kann.
		
		//Tische/Sessel hinzuf�gen manuell
		r1.addTische(t);
		r1.addTische(t2);
		r2.addTische(t2);
		r2.addSessel(s4);
		r2.addSessel(s3);
		System.out.println(r1.getTischListe());
		System.out.println(r1.getSesselListe());
		
		//Methode besesseln() testen
		System.out.println("Raum 1 besesseln");
		r2.besesseln(4);
		System.out.println();
		System.out.println("Raum 2 besesseln:");
		r1.besesseln(5);
		System.out.println();
		
		//Methode inventarSessel() testen
		System.out.println("So viele Sessel von diesen Materialien gibt es aktuell im Lager: ");
		Sessel.inventarSessel();
		System.out.println();

		//Methode fengShui() testen
		System.out.println(r2.getMoebelListe());
		System.out.println();
		r2.fengShui(Material.Holz, Design.antick);
		
		//Print von den R�umen mit den Sesseln usw...
		System.out.println();
		r1.printDaten();
		System.out.println();
		r2.printDaten();
		System.out.println();
		
		//Methode ausraeumen testen -> 4 sind den r�umen r1 und r2 zugeordnet ... weitere 4 mit design antick werden in der methode inventarSessel() erzeugt...
		//daher lager aktuell mit 8 m�bel(design antick).
		r2.ausraeumen(Design.antick);
		r1.ausraeumen(Design.antick);
		System.out.println("lagergschichtln: " + Raum.getLager());
		
	}

}
