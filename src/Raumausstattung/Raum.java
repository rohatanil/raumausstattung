package Raumausstattung;

import java.util.ArrayList;
import java.util.Iterator;

public class Raum {


	private int nutzflaeche;
	private final String raumName;
	private final int maxSesselimRaum = 3;
	private final int maxTischQuote = 5; // 5 hier f�r 20%
	private final String raumID;
	private static int counter = 0;

	private static ArrayList<Raum> raumListe = new ArrayList<>();
	private ArrayList<Tisch> tischListe = new ArrayList<>(); //jeder Raum hat seine eigenen Tische
	private ArrayList<Sessel> sesselListe = new ArrayList<>(); //jeder Raum hat seine eigenen Tische
	private static ArrayList<Moebel> moebelLager = new ArrayList<>();
	private static ArrayList<Moebel> lager = new ArrayList<>();
	
	

	
	public Raum(int nutzflaeche, String raumName) {
		
		if(nutzflaeche == 0) {
			throw new IllegalArgumentException("Die Nutzfl�che von einem Raum kann nicht 0 sein!");
		}
			this.nutzflaeche = nutzflaeche;
		
		//mit length kann ich hie rnicht arbeiten wiel leerzeichen dann auch akzeptabel w�re.
		// aber mit trim frisst er links und rechts alles weg -> also w�re alles wieder 0 bei 
		// einer angabe wie new Raum("    ") => 0
		if(raumName == null || raumName.trim().length() == 0) { 
				throw new IllegalArgumentException("Raumname darf nicht leer sein!");
		}
		this.raumName = raumName;
		this.raumID = "R" + counter;
		counter++;
		if(raumCheck(this)) {
			raumListe.add(this); //zuerst wird gecheckt ob der Raum bereits mit dem selben namen existiert. wenn ja dann wirds nicht eingespeichert.
		}else {
			throw new IllegalArgumentException("Dieser Raum existiert bereits. Bitte anderen Namen eingeben.");
		}
	}
	
	private boolean raumCheck(Raum r) {
		for (Raum raum : raumListe) {
			if(r.getRaumName().equals(raum.getRaumName())) {
				System.out.println("Raum '"+ r.getRaumName() + "' existiert bereits.");
				return false;
			}
		}
		return true;
	}

	public void addTische(Tisch t) {
		tischListe.add(t);
		moebelLager.add(t);
	}
	
	public void addSessel(Sessel s) {
		sesselListe.add(s);
		moebelLager.add(s);
	}

	public void besesseln(int anzahl) {
		int anzahlSessel = this.getAnzahlSessel();
		int counter = 0;
		int diff = maxSesselimRaum - anzahlSessel;
		int raumgroesse = this.getNutzflaeche();
		int erlaubteSesselimRaum = raumgroesse * 3;
		
		while(anzahlSessel < erlaubteSesselimRaum) {
			if(Sessel.getAlleSesselImLager().isEmpty()==false) {
				this.addSessel(new Sessel(Material.Holz, Design.antick));
			}else {
				System.out.println("Im Lager befinden sich keine Sessel mehr");
			}
			counter++;
			anzahlSessel++;
		}
		System.out.println("Es wurden " + counter + " Sessel hinzugef�gt.");
		
		/**while(anzahlSessel < maxSesselimRaum) {
			System.out.println("Aktuell w�ren noch Platz f�r " + diff + " Sessel");
			if(Sessel.getAlleSesselImLager().isEmpty()==false) {
				this.addSessel(new Sessel(Material.Holz, Design.antick));
			}else {
				System.out.println("Im Lager befinden sich keine Sessel mehr");
			}
			anzahlSessel++;
			counter++;
			diff--;
		}*/
		
	}
	
	public void fengShui(Material material, Design design) {
		boolean passt = false;
		for(Moebel m : moebelLager) {
			if(m.getDesign().equals(design) && m.getMaterial().equals(material)) {
				passt = true;
			}else {
				passt = false;
				break;
			}
		}
		if (passt) {
			System.out.println("Alle M�bel haben das selbe Material: '" + material +"' und das selbe Design: '" + design +"'");
		}else {
			System.out.println("Die M�bel in diesem Raum haben nicht das selbe Material und Design.");
		}
	}
	
	public void ausraeumen(Design design) {
		for(Moebel m : moebelLager) {
			if(m.getDesign().equals(design)) {
				lager.add(m);
				moebelLager.remove(m.getDesign().equals(design));
			}
		}
	}
	
	public int getAnzahlSessel(){
		int anzahl = 0;
		for(Sessel s : sesselListe) {
			anzahl++;
		}
		return anzahl;
		
	}
	
	public void printDaten() {
		System.out.println("RaumID: " + this.getRaumID());
		System.out.println("Raumfl�che: " + this.getNutzflaeche()+"m�");
		System.out.println("Tische: " + this.getTischListe());
		System.out.println("Sesselz: " + this.getSesselListe());
	}
		
	
	//---------------------------------------------------------------------------------------
	//getter und setter
	public String getRaumID() {
		return raumID;
	}

	public int getNutzflaeche() {
		return nutzflaeche;
	}

	public void setNutzflaeche(int nutzflaeche) {
		this.nutzflaeche = nutzflaeche;
	}

	
	public String getRaumName() {
		return raumName;
	}

	public ArrayList<Raum> getRaumListe() {
		return raumListe;
	}
	
	public ArrayList<Moebel> getMoebelListe() {
		return moebelLager;
	}

	public ArrayList<Tisch> getTischListe() {
		return tischListe;
	}
	
	public ArrayList<Sessel> getSesselListe() {
		return sesselListe;
	}
	
	public static ArrayList<Moebel> getMoebelLager() {
		return moebelLager;
	}

	public static void setMoebelLager(ArrayList<Moebel> moebelLager) {
		Raum.moebelLager = moebelLager;
	}

	public static ArrayList<Moebel> getLager() {
		return lager;
	}

	public static void setLager(ArrayList<Moebel> lager) {
		Raum.lager = lager;
	}
	

	@Override
	public String toString() {
		return "Raum [nutzflaeche=" + nutzflaeche + ", " + (raumName != null ? "raumName=" + raumName + ", " : "")
				+ "maxSesselimRaum=" + maxSesselimRaum + ", maxTischQuote=" + maxTischQuote + ", "
				+ (raumID != null ? "raumID=" + raumID + ", " : "")
				+ (tischListe != null ? "tischListe=" + tischListe + ", " : "")
				+ (sesselListe != null ? "sesselListe=" + sesselListe + ", " : "")
				+ (moebelLager != null ? "moebelListe=" + moebelLager : "") + "]";
	}
	

}
