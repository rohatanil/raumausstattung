package Raumausstattung;

public class Tisch extends Moebel {

	private int nutzflaeche;
	private double laenge;
	private double breite;

	public Tisch(Material material, Design design, int nutzflaeche, double laenge, double breite) {
		super(material, design);
		if(laenge < 1 || (breite < 0.4 || breite > 1 )) {
			throw new IllegalArgumentException("L�nge darf nicht kleiner als 1m sein und die Breite nicht kl. als 0,4m oder gr��er als 1m");
		}
		this.nutzflaeche = nutzflaeche;
		this.laenge = laenge;
		this.breite = breite;
	}

	@Override
	public String vorzeichen() {
		return "T";
	}

	@Override
	public void printDaten() {
		System.out.println("ID: " + getMoebelID());
	}
	
	@Override
	public String toString() {
		return "Tisch [nutzflaeche=" + nutzflaeche + ", laenge=" + laenge + ", breite=" + breite + ", "
				+ (getMaterial() != null ? "getMaterial()=" + getMaterial() + ", " : "")
				+ (getDesign() != null ? "getDesign()=" + getDesign() : "") + "]";
	}

	
	public int getNutzflaeche() {
		return nutzflaeche;
	}

	public double getLaenge() {
		return laenge;
	}

	public double getBreite() {
		return breite;
	}
	

}
